package com.example.funnystories.view.base;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.funnystories.App;
import com.example.funnystories.StorageCommon;
import com.example.funnystories.view.OnActionCallback;

abstract public class BaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected OnActionCallback mCallback;

    public void setmCallback(OnActionCallback mCallback) {
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return viewHolder(parent, viewType);
    }

    protected abstract RecyclerView.ViewHolder viewHolder(ViewGroup parent, int viewType);


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        onBindView(holder, position);
    }

    protected abstract void onBindView(RecyclerView.ViewHolder holder, int position);

    @Override
    public int getItemCount() {
        return getCount();
    }

    protected abstract int getCount();

    protected StorageCommon getStorageCommon() {
        return App.getInstance().getStorageCommon();
    }

}
