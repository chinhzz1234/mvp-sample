package com.example.funnystories.view.base;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.funnystories.App;
import com.example.funnystories.R;
import com.example.funnystories.StorageCommon;
import com.example.funnystories.presenter.BasePresenter;
import com.example.funnystories.view.OnActionCallback;

import java.lang.reflect.Constructor;

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements OnActionCallback {
    protected T mPresenter;
    protected Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            excuteHandler(msg);
            return false;
        }
    });

    protected void excuteHandler(Message msg) {

    }

    protected StorageCommon getStorageCommon() {
        return App.getInstance().getStorageCommon();
    }

    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        initPresenter();
        initView();
    }

    protected abstract void initPresenter();

    protected abstract void initView();

    protected abstract int getLayoutId();

    public <T extends View> T findViewById(int id, View.OnClickListener event) {

        return findViewById(id, event, null);
    }

    public <T extends View> T findViewById(int id, View.OnClickListener event, Typeface typeface) {
        T v = super.findViewById(id);
        if (event != null) {
            v.setOnClickListener(event);
        }
        if (v instanceof TextView) {
            if (typeface == null) {
                typeface = App.getInstance().getFontRegular();
            }
            ((TextView) v).setTypeface(typeface);
        }
        return v;
    }

    public <T extends View> T findViewById(int id, Typeface typeface) {
        return findViewById(id, null, typeface);
    }

    protected void showFragment(String tag) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName(tag);
            Constructor<?> ctor = clazz.getConstructor();
            BaseFragment object = (BaseFragment) ctor.newInstance();
            object.setmCallback(this);
            getSupportFragmentManager().beginTransaction().replace(R.id.fr_main, object).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void callback(String key, Object data) {

    }
}
