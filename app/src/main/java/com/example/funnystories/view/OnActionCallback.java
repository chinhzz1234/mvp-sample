package com.example.funnystories.view;

public interface OnActionCallback {
    void callback(String key, Object data);
}
