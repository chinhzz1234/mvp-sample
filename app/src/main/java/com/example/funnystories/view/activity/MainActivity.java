package com.example.funnystories.view.activity;

import com.example.funnystories.R;
import com.example.funnystories.view.base.BaseActivity;

public class MainActivity extends BaseActivity {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initPresenter() {
    }

    @Override
    protected void initView() {
    }

    @Override
    public void callback(String key, Object data) {

    }
}
